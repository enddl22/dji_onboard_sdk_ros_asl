# Dynamic system identification and control of a cost-effective VTOL MAV #

This project contains 6 software packages:

* [Onboard-SDK-ROS](https://bitbucket.org/enddl22/dji_onboard_sdk_ros_asl/src/47f1799bc35d474e5507598c0d0f40fca46cc292/Onboard-SDK-ROS/?at=master), a modified version of DJI Onboard-SDK-ROS. The main different to the [original Onboard-SDK-ROS](https://github.com/dji-sdk/Onboard-SDK-ROS) is utilizing different functions for sending commands. The original uses ROS service call to send commands that are highly not recommended whereas we directly call the send command function exploiting a serial interface. A small delay in control signal makes a huge difference in the control performance.

* [mav_control](https://github.com/ethz-asl/mav_control_rw), A Linear-Model Predictive Control (MPC) used in the paper for position control. Inputs are dynamic models of the vehicle and the desired pose and the node calculate the roll and pitch angles and yaw rate to reach the goal. (Under-developing)

* [System identification tools](https://bitbucket.org/enddl22/dji_onboard_sdk_ros_asl/src/171b49e149102ca52222d405c4cd312e47d4027b/m100_system_identification/?at=master), MATLAB scripts to perform system identification for 1st and 2nd dynamics models. It comes with example bag files that contain Vicon motion captured position and orientation, onboard IMU measurement, and input commands.

* [control_toolbox](https://bitbucket.org/enddl22/dji_onboard_sdk_ros_asl/src/47f1799bc35d474e5507598c0d0f40fca46cc292/control_toolbox/?at=master), For height velocity PID control of M100.

* [ros-keyboard](https://bitbucket.org/enddl22/dji_onboard_sdk_ros_asl/src/a682f69f6f08644cc823671a4fa3ee534ac43dcf/ros-keyboard/?at=master), the keyboard interface for obtaining serial permission through the SDK.

* [rotors_joy_interface](https://bitbucket.org/enddl22/dji_onboard_sdk_ros_asl/src/e862bb5aa75dd52904b6827c5b4c0f742cd1f691/rotors_joy_interface/?at=master), a ROS node for sending commands through the serial port using a joystick. Note that it is different to use a transmitter that has the highest priority.

## More details and explanations can be found the link below. ##

[Documentation](https://docs.google.com/document/d/1WLlb36hU_WYNic4xdhRVls3XcNmlFRTRmHVcRiIkdzc/edit?usp=sharing)

## Demo video ##

https://youtu.be/0IDzo1D1DhA